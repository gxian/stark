\section{Solution}
	\subsection{Gesture Language}
		The gesture language for this interface was designed to allow all the actions
		mentioned in section \ref{sec:leap_int} to be accessible. The following
		aspects were balanced in the design of the language:
		
		\begin{itemize}
		  \item Intuitiveness
		  \item Ergonomics
		  \item Detection reliability with the LEAP
		  \item Ease and speed of mathematical analysis
		\end{itemize}
		
		This language allows simultaneous viewport navigation and sculpt tool
		operation. This is achieved by having the left hand gestures
		navigate the viewport and right hand gestures operate the sculpt tool.
		Unlike GaFinC which uses two handed gestures, this language measures finger
		positions to determine the action the user intends with that hand.
		
		To distinguish between the left and right hand, the \textit{x} co-ordinate of
		the LEAP's measurements is used. The hand detected with an \textit{x} value
		of less than 0 is the left hand, the hand detected with an \textit{x} value equal to
		or greater than 0 is a right hand.
	
		\subsubsection{View Manipulation}
			\begin{figure}[h]
				\centering
				\fbox{\includegraphics[width=240pt]{blender_camera_nomeclature.png}}
				\caption{Blender's camera reference frame conventions and what vectors
				represent ($w$ is world reference frame, $c$ is camera rig center reference
				frame and $i$ is image plane reference frame)}
				\label{fig:blender_ref_frames}
			\end{figure}
				
			Blender's virtual camera ``rig'' setup for viewport navigation is shown in
			Fig. \ref{fig:blender_ref_frames}. Note that the camera rig's center $c$
			is in front of the image plane, which allows tumbling to be a simple
			rotation of the rig about $c$ without any translation component. Also,
			dollying just involves changing $d_c^i$.
			
			For viewport navigation, the number of fingers outstretched on the left
			hand determines the action that is initiated. There are four distinct
			gestures and therefore actions for viewport navigation:
			
			\begin{itemize}
			  \item Idle: Initiated when all fingers are retracted and forms a fist
			  \item Tracking: Initiated when two fingers are outstretched and executed
			  when hand is moved
			  \item Tumbling: Initiated when one finger is outstretched and executed hand
			  is moved
			  \item Dollying: Initiated when two fingers are outstretched and executed
			  when spread between the outstretched fingers change
			\end{itemize}
			
			\begin{figure}[h]
				\centering
				\fbox{\includegraphics[width=240pt]{combo_gesture_illustration.png}}
				\caption{LEAP interface gestures for tracking (lower left), tumbling
				(top center) and dollying (lower right)}
				\label{fig:new_gest_nav}
			\end{figure}
			
			Initiating the idle action stops the current action. All movements on the
			left hand are ignored during this action.
			
			Tracking has three degrees of freedom. During a tracking action Eqn.
			\ref{eqn:track_alg} is evaluated at every time step to determine
			the new position of the camera.
			
			\begin{equation}	\label{eqn:track_alg}
				\vec{s_{w}^{c\prime}} = \vec{s_{w}^{c}} -
				\mat{T}_{w}^{c} \vec{v_{palm}}k\Delta t
			\end{equation}
			
			Eqn. \ref{eqn:track_alg} is basically Eqn. \ref{eqn:cam_newpos} but subtracts
			the movement vector instead of adding, so the camera moves in the opposite
			direction to the hand (akin to ``natural'' scrolling on a trackpad).
			Multiplying $\vec{v_{palm}}$ by the rotation matrix $\mat{T}_w^c$ (obtained
			by Rodrigues formula) causes the camera rig to move with respect to the local
			reference frame ($c$) instead of the world's ($w$). Together, this gives the
			the user the impression that they are grabbing and movng the workpiece, which
			is much more intuitive than GaFinC's behavior where the camera moves in the same
			direction as the hand.
			
			Tumbling has two degrees of freedom:
			\begin{itemize}
			  \item Vertical component of palms's velocity determines the amount of
			  tumbling about the $c_x$ axis
			  \item Horizontal component of the palm's velocity determines the amount of
			  tumbling about the $w_z^\prime$ axis (\textit{z} axis in world reference frame but origin
			  shifted to $c$)
			\end{itemize}
			
			At every time step, two quaternions, one for each tumbling component is
			generated, as shown in Eqn. \ref{eqn:quat_x_tumb} and Eqn.
			\ref{eqn:quat_z_tumb}.
			
			\begin{equation}	\label{eqn:quat_x_tumb}
				q_x = \vec{(v_{palm})_z}k\Delta t - 1i + 0j + 0k 
			\end{equation}
			
			\begin{equation}	\label{eqn:quat_z_tumb}
				q_z = \vec{(v_{palm})_x}k\Delta t + 0i + 0j + 1k
			\end{equation} 
			
			The new orientation of the camera rig is found by multiplying the quaternion
			representing the previous orientation by the per-axis quaternions as shown in
			Eqn. \ref{eqn:quat_net}. Note that with quaternions, the order defines
			whether the rotation is performed about the local or global reference frame.
			
			\begin{equation}	\label{eqn:quat_net}
				q_{cam}^\prime = q_z \times q_{cam} \times q_x 
			\end{equation} 
			
			Though this gesture may not seem as intuitive compared to revolving one's
			fingers around each other, this gesture is more ergonomic due to the limited
			mobility of the wrist and it is also easier for the LEAP to reliably detect
			due to finger self-occlusion (see section \ref{sec:self-occ}). The axes
			restrictions make the workpiece appear as if it were on a turntable, which is intuitive for
			experienced digital sculpters that use a mouse.
			
			Dollying has one degree of freedom. At every time step, the new distance
			between the image plane and the camera rig's center (see Fig.
			\ref{fig:blender_ref_frames}) is calculated by Eqn. \ref{eqn:dolly}.
			Note that the $P$ reference frame (see Fig. \ref{fig:P_ref_frame}) is rotated
			such that the \textit{x} component velocity of the fingers is always equal to the magnitude of that
			vector, this makes the gesture orientation independent. Key behavioral
			difference from GaFinC's algorithm is that this uses a difference which
			results in a linear relationship between finger velocitiy and dolly rate and
			GaFinC uses a quotient which results in a non-linear relationship. This
			results in a possible sacrifice in fine control but should be more
			intuitive. Users can ``zoom to cursor'' just like in GaFinC by moving their
			hand while changing the spread of the outstretched fingers. As the LEAP is
			very accurate with hand positions, this allows for more precise control over
			the cursor location compared to GaFinC's eye tracking which was noted to be
			less accurate than a mouse.
			
			\begin{equation}	\label{eqn:dolly}
				d_c^{i\prime} = d_c^i + \mat{T}_w^{P}(\vec{v_w^{f0}} -
				\vec{v_w^{f1}})_xk\Delta t
			\end{equation}
			
			\begin{equation}
				\vec{P} = \vec{s_w^{f0}} - \vec{s_w^{f1}} 
			\end{equation}
		
			
			\begin{figure}[h]
				\centering
				\fbox{\includegraphics[width=160pt]{dolly_P_ref_frame.png}}
				\caption{Illustration of the rotated $P$ reference frame}
				\label{fig:P_ref_frame}
			\end{figure}
			
			Note that palm position is used for motion control in these above gestures.
			This is because finger movements are used to delimit the gestures. Number of
			fingers outstretched is an effective and efficient way to delimit gestures
			\cite{art:freehand-gestures}, particularly with the LEAP which is designed
			for detecting fingers.
			
			
		\subsubsection{Sculpt Tool Control}	\label{subsubsec:sculpt_tool_ctl}
			In the absence of right hand gestures (excluding idle gesture which is the
			same as the left; no fingers outstretched), the sculpt tool cursor does not
			appear to move relative to the user's view i.e. the image plane's
			transformations parents the tool's. To achieve this, during a viewport
			navigation action, Eqn. \ref{eq:tool_world_pos} is evaluated at every time
			step to update the position of the tool so that it appears stationary
			relative to the image plane at all times. The sculpt tool is moved (all
			sculpt tool transformations in section \ref{subsubsec:sculpt_tool_ctl} are
			described in the image plane's reference frame unless otherwise stated) by
			outstretching at least one finger on the right hand and moving them. The tool
			will move proportional to the average velocity vector
			($\bar{\vec{v_{finger}}}$) of the outstretched fingers as shown in Eqn.
			\ref{eq:rel_tool_cam}.
			
			\begin{equation}	\label{eq:rel_tool_cam}
				\vec{s_{i}^{tool\prime}} = \vec{s_{i}^{tool}} +
				\bar{\vec{v_{finger}}}k\Delta t
			\end{equation}
			
			\begin{equation}	\label{eq:tool_world_pos}
				\vec{s_{w}^{tool}} = \vec{s_{w}^{c}} + \mat{T}_{w}^{c} \left(
				\begin{bmatrix}
					0 \\ 0 \\ d_c^i
				\end{bmatrix}
				+ \vec{s_{i}^{tool}} \right)
			\end{equation}
			
			The tool's orientation tracks the finger's orientation absolutely. Eqn.
			\ref{eqn:finger_track_world} finds the quaternion that describes the tool's
			orientation in the world reference frame ($q_w^c$ is the quaternion decribing
			the orientation of the camera rig with respect to the world reference frame).
			The LEAP returns the finger's orientation as a vector representing the
			direction it is pointing ($\bar{\hat{p}}$). The algorithm described in
			Eqn. \ref{eqn:rot_w} and Eqn. \ref{eqn:rot_v} are used to find the quaternion
			that represents finger's orientation using vector $\bar{\hat{p}}$ (as seen in
			Eqn.
			\ref{eqn:fing_rot_w} and Eqn. \ref{eqn:fing_rot_v}).
			
			\begin{equation}	\label{eqn:finger_track_world}
				q_{w}^{tool} = q_{w}^{c} \cdot q_{finger}
			\end{equation}
			
			\begin{equation}
				q_{finger} = w + \hat{v}   
			\end{equation}
			
			\begin{equation}	\label{eqn:fing_rot_w}
				w = \sqrt{1 + \hat{r} \cdot \bar{\hat{p}}}
			\end{equation}
			
			\begin{equation}	\label{eqn:fing_rot_v}
				\hat{v} = \hat{r} \times \bar{\hat{p}}
			\end{equation}
			
			$\bar{\hat{p}}$'s orientation is measured with respect to $\hat{r}$. The
			vector $\hat{r}$ is set to 
			$\begin{bmatrix}
				0 & 1 & 0				
			\end{bmatrix}$
			as the finger cannot ergonomically rotate more than $\pi$ on either side of
			this vector in the LEAP's reference frame \cite{art:ergo}. As the case where
			$\abs{\theta_{min}} \geq \pi$ is unlikely to occur and irrelevant to
			user experience, there is no need for special handling (see section
			\ref{subsec:rotated_ref_frame}). The sculpt tool will deform the workpiece
			when the tip of the tool meets the workpiece. The effect on the workpiece,
			whether it is material reduction or addition will depend on the number of
			fingers outstretched on the right hand. A single finger outstetched will
			remove material whereas two fingers will add material.
			
			

