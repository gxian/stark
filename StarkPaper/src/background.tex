\section{Related Research and Projects}
	\subsection{Gesture Language and Recognition}
		Song et al. \cite{art:CAD} developed a two handed gesture language for
		CAD\footnote{Computer Aided Design} software viewport navigation called
		GaFinC. The gestures are illustrated by Fig. \ref{fig:gest_nav}.
		
		\begin{figure}[h]
			\centering
			\fbox{\includegraphics[width=240pt]{combo_hands.png}}
			\caption{GaFinC gestures for tracking (left), tumbling (center) and dollying
			(right)}
			\label{fig:gest_nav}
		 \end{figure}
		
		Tracking is gestured by one hand with pinched fingers as shown on the
		lower left in Fig. \ref{fig:gest_nav}. As seen in Eqn. \ref{eqn:cam_newpos}, a
		discrete vector integration is used to find the new camera position
		($\vec{A'}$). Move amount is relative, the sensitivity of the
		gesture can be tuned by changing the constant $k$.
		\begin{equation}	\label{eqn:cam_newpos} 
			\vec{A'_{cam}} = \vec{A_{cam}} + \vec{dP_{cam}}	
	    \end{equation}
			
		\begin{equation} \label{eqn:cam_dP}
			\vec{dP_{cam}} = k(\vec{P_f} - \vec{P_i})
		\end{equation}
		
		Tumbling is gestured by revolving both hands with pinched fingers around each
		other as shown in the top center in Fig. \ref{fig:gest_nav}. Eye tracking
		determines the center of rotation, $C$ (eye tracking in this view can only
		select locations in front of the image plane, if it were behind, it would a be
		panning action). The rotational displacement between $P_f$ and $P_i$ as
		represented by three Euler angles (see Eqn. \ref{eqn:quaternion}) can be
		found by Eqn. \ref{eqn:theta_x} (use $P_{xz}$ for $\theta_y$, use $P_{xy}$ for
		$\theta_z$). Many 3D graphics and CAD software has Euler angles as one of its
		representations for rotation.
		
		\begin{equation} \label{eqn:quaternion}
			\theta_{rot} = \theta_{x} + \theta_{y} + \theta_{z}
		\end{equation}
		
		\begin{equation} \label{eqn:theta_x}
			\theta_x
			=\arcsin\left(\frac{\vec{(P_i)_{yz}}\cdot\vec{(P_f)_{yz}}}{\norm{\vec{(P_i)_{yz}}}\norm{\vec{(P_f)_{yz}}}}\right)
		\end{equation}
		
		Dollying is gestured by having index fingers on both hands outstretched
		then moving hands together or apart as seen on the right of Fig.
		\ref{fig:gest_nav} to dolly out or in respectively. Though dollying
		constrains the camera along its optical axis, GaFinC had a ``zoom to
		cursor'' feature where the cursor location was determined by eye tracking. This
		effectively performed a tracking action constrained along the camera's image
		plane simultaneously with dollying. The distance to dolly the camera ($l$)
		is proportional to the quotient between the finger's distance at the end
		and the beginning of the gesture as seen in Eqn. \ref{eqn:l}. Move amount is
		relative, sensitivity of the gesture can be tuned by changing the constant $k$.
		
		\begin{equation} \label{eqn:l}
			l = k\left(\frac{\norm{\vec{P_f}}}{\norm{\vec{P_i}}}\right)
		\end{equation}
		
		The limitation of GaFinC is that it uses two hands just for viewport
		navigation, so the user cannot sculpt and navigate the viewport simultaneously
		and thus not all of the actions listed in section \ref{sec:leap_int} will be
		available in this interface. However, the algorithms are described in terms
		of generic points and thus should be applicable to finger gestures. Song et
		al. does not explore in detail the method used to determine finger positions
		and whether they are pinching (an essential component of his gesture
		language), the LEAP has this ability.
		
		GaFinC's viewport navigation accuracy was inferior to that of the mouse.
		It also performed the worst at tracking. Tracking accuracy was measured to be
		7.6\% compared to 3.5\% acheived by the mouse. Tumbling error was measured to
		be 5.2\degree\  compared to 3.6\degree\  achieved by the mouse.
		
		
	\subsection{Rotated Reference Frames}	\label{subsec:rotated_ref_frame}
		Intuitively, users expects actions to track their hand movements in
		the reference frame of the virtual camera rather than the world reference
		frame. However, the software requires transformations to be specified in the
		latter. Therefore, measurements from the gesture sensor must be processed.
		
		Rodrigues formula (Eqn. \ref{eqn:rodrigues}) computes a matrix such that when
		a vector is left multiplied by it (Eqn. \ref{eqn:vect_trans}), the result is
		the equivalent vector described with respect to the reference frame rotated
		about unit vector $\hat{u}$ by angle $\theta$ \cite{art:rotations}.
		
		\begin{equation}	\label{eqn:rodrigues}
			\mat{Q}(\theta, \hat{u}) = \cos{\theta}\mat{I} + \sin{\theta}\mat{U} + (1 -
			\cos{\theta})\mat{U}^2
		\end{equation}
		
		\begin{equation}	\label{eqn:vect_trans}
			\vec{P_{W}^{i}} = \mat{Q}_{W}^{C}(\theta, \hat{u}) \cdot \vec{P_{C}^{i}} 
		\end{equation}
		
		\begin{equation}
			\mat{U} = \begin{bmatrix}
				0 & -u_z & u_y \\
				u_z & 0 & -u_x \\
				-u_y & u_z & 0
			\end{bmatrix}
		\end{equation}
		
		This is useful because many 3D graphics software has angle-axis as a
		representation for rotations \cite{prc:fast-quat}.
		
		There is an algorithm which is found in many graphics engines such as Ogre3D
		which finds the quaternion\footnote{An extension of complex numbers that has
		three imaginary components instead of one} which represents the shortest
		rotation to get from unit vector $\hat{v_1}$ to unit vector $\hat{v_2}$
		\cite{web:lolengine}. This algorithm treats the quaternion as the sum of a
		scalar (represents the real component of the quaternion) and a unit vector
		(represents the imagninary components of the quaternion) as seen in Eqn.
		\ref{eqn:quat_rep}.
		
		\begin{equation}	\label{eqn:quat_rep}
			q = w + xi + yj + zk = w + \hat{v}
		\end{equation}
		
		Then $w$ and $\hat{v}$ can be found by Eqn. \ref{eqn:rot_w} and Eqn.
		\ref{eqn:rot_v}.
		
		\begin{equation}	\label{eqn:rot_w}
			w = \sqrt{1 + \hat{v_1} \cdot \hat{v_2}} 
		\end{equation}
		
		\begin{equation}	\label{eqn:rot_v}
			\hat{v} = \hat{v_1} \times \hat{v_2}
		\end{equation}
		
		Unfortunately, this algorithm needs special handling when $\abs{\theta_{min}}
		\geq \pi$.
		
			
	\subsection{Related Existing Software}
		Autodesk has created ``Leap Motion Plug-in for Autodesk\textregistered\
		Maya\textregistered\ 2014'' \cite{web:Autodesk-Plugin} for a competitor
		program to Blender called Maya. This plugin not only has mesh sculpting (see
		Fig. \ref{fig:Maya_LEAP}), but a variety of other features and has a Python
		API for user customizaton.
		
		\begin{figure}[h]
			\centering
			\includegraphics[width=200pt]{maya_leap.jpg}
			\caption{LEAP being used to sculpt mesh in Autodesk Maya}
			\label{fig:Maya_LEAP}
		\end{figure}
		
		However this plugin is not usable as a fully-featured sculpting tool without
		extensive user customization. Several actions listed in section
		\ref{sec:leap_int} still require a mouse, namely viewport navigation and tool
		attribute selection. Omitting the mouse for all actions may not improve
		intuitiveness or productivity but doing so for the key aspects mentioned in
		section \ref{sec:leap_int} should.
		
		BlenderArtists forum has a few members who are working on LEAP integration
		with Blender. Many have released their source code to the public.
