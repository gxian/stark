\section{Testing Methodology}
	The LEAP has several hardware intrinsic aspects that will impact the
	performance of this interface including:
	\begin{itemize}
	  \item Magnitude of noise
	  \item Latency
	  \item Angle of finger self-occlusion
	\end{itemize}
	
	\subsection{Noise}
		Magnitude of noise was measured by getting the user to hold their hand in the
		detectable region of the LEAP as stationary as possible. The velocity (this
		quantity affects many actions) for several hundred frames was then recorded.
		The magnitude of the noise is then quantified as the root mean square (RMS) of
		the data set (Eqn. \ref{eqn:RMS}).
		
		\begin{equation}	\label{eqn:RMS}
			RMS = \sqrt{\frac{1}{n}\sum\limits_{i=1}^{n}\norm{\vec{v_{palm}}}^2}			
		\end{equation}
		
	\subsection{Latency}
		Latency is the time difference between user input and feedback output
		(display update). Between these two events, operations such as data processing
		and graphics rendering must completed. The speed of these operations are affected by
		computer hardware, choice of algorithms and programming language used.
		
		The latency was measured by recording the times at these following events:
		
		\begin{itemize}
		  \item Beginning of the frame
		  \item Completion of data processing
		  \item Completion of graphics rendering
		\end{itemize}
		
		Times obtained for several frames were averaged. Frames in which there were no
		hands detected or where both hands gestured an idle action were ignored as the
		user experience will not be affected.
		
		The operating system clock was used to time these operations. The computer
		hardware used was an Intel i5-430M CPU (mainly used in data processing)
		with an AMD Mobility Radeon HD 5650 GPU (mainly used in graphics rendering).
		
	\subsection{Finger Self-Occlusion}	\label{sec:self-occ}
		\begin{figure} [h]
			\centering
			\includegraphics[width=200pt]{leap_motion_visualize.png}
			\caption{LEAP Motion Visualizer program}
			\label{fig:LEAP_motion_visualizer}
		\end{figure}
		The LEAP has only one flat detection plane. Therefore fingers will obscure
		each other when the vector scribed between them is close to 90\textdegree\  from
		the detection surface (this angle is termed ``finger angle''). To determine when
		these occured, the LEAP Motion Visualizer program (see Fig.
		\ref{fig:LEAP_motion_visualizer}) was used. This program renders a symbolic
		graphical construction of the hand based on data received from the LEAP. When
		fingers disappear from the graphics render, an occlusion event has occured.
		
		This test was conducted using only two fingers outstreteched that were
		approximately 17mm thick and spaced approximately 8mm apart at the tip. The fingers
		were rotated about each other from a finger angle of 0\textdegree\  to
		180\textdegree\  slowly.
		
		In the frames that were received just before an occlusion event, the position
		vectors of the two fingers ($\vec{s_{finger}}$) were submitted to Eqn.
		\ref{eqn:occ_angle} to find the occlusion angle. The test was repeated at
		different locations in the LEAP's detectable region.
		
		\begin{equation}	\label{eqn:occ_angle}
			\theta = \arctan\left(\frac{(\vec{s_{finger0}} -
			\vec{{s_{finger1}}})_z}{(\vec{s_{finger0}} - \vec{{s_{finger1}}})_x}\right)
		\end{equation}
		
	\subsection{User Tests}
		User tests were performed to determine the accuracy of the LEAP
		interface in comparison to a mouse. The accuracy tests were:
		
		\begin{enumerate}
		  \item Tracking viewport a fixed number of units	\label{enum:test-tracking}
		  \item Tumbling viewport by 90\degree				\label{enum:test-tumbling}				
		\end{enumerate}
		
		These operations were chosen because their errors are easy to measure
		accurately.
		
		Test \ref{enum:test-tracking} started with the virtual camera rig at the
		origin of the viewport. The user was given visual feedback consisting of a
		marker on the image plane in which the user must align the object in the
		viewport with. The location of the camera was then compared between where it
		should be, operation as performed by a mouse (control) and operation
		as performed by the LEAP interface. The results from three tests were
		averaged.
		
		Test \ref{enum:test-tumbling} started with the virtual camera with an
		orientation as represented by quaternion $(1 + 0i + 0j + 0k)$. The user was
		given visual feedback consisting of a cube which at the beginning of the
		operation has one of its faces parallel to the image plane. The user must then
		tumble the viewport such that another face of the same cube appears parallel
		to the image plane. Users had to perform this operation four times, left and
		right for both \textit{x} and \textit{y} axis. Results from these tests were
		averaged.
