"""
Declares custom hand types with some mathematical processing
"""

__author__ = "George Xian"
__version__ = "0.1"
__email__ = "george_xian@live.com"
__status__ = "development"

# IMPORT MODULES
import numpy

# FUNCTION DEFINTION
def cart2sph3D(vect):
    """Converts a 3 element array from cartesian to spherical co-ordinates"""
    r = numpy.linalg.norm(vect, 2)
    theta = numpy.arccos(vect[2]/r)
    try:
        phi = numpy.arctan(vect[1]/vect[0])
    except ZeroDivisionError:
        if vect[1] >= 0:
            phi = numpy.pi/2
        else:
            phi = -numpy.pi/2
    
    return numpy.array([r, theta, phi])

def sph2cart3D(vect):
    """Converts a 3 element array from spherical to cartesian co-ordinates"""
    r, theta, phi = (vect[0], vect[1], vect[2])
    x = r*numpy.cos(theta)*numpy.sin(phi)
    y = r*numpy.sin(theta)*numpy.sin(phi)
    z = r*numpy.cos(phi)
    
    return numpy.array([x, y, z])

def quat_between(vect1, vect2):
    """Quaternion to transform vect1 to vect2"""
    xyz = numpy.cross(vect1, vect2)
    v1_mag = numpy.linalg.norm(vect1, 2)
    v2_mag = numpy.linalg.norm(vect2, 2)
    w = numpy.sqrt(v1_mag**2 * v2_mag**2 + numpy.dot(vect1, vect2)) 
    
    return numpy.hstack((w, xyz))

def eul_angle(vect1, vect2):
    """Euler angle transform vect1 to vect2
    Note: I really want to deprecate this as I don't actually know what this is doing"""
    
    a, b, c = tuple(vect2 - vect1)
    
    if b == 0:
        # 90 degree cases
        if c != 0:
            theta_x = numpy.pi/2
        else:
            theta_x = 0
    else:
        # normal case
        theta_x = numpy.arctan(c/b)
        
    if a == 0:
        # 90 degree cases
        if c != 0:
            theta_y = numpy.pi/2
        else:
            theta_y = 0
            
        if b != 0:
            theta_z = numpy.pi/2
        else:
            theta_z = 0
    else:      
        # normal case
        theta_y = numpy.arctan(c/a)
        theta_z = numpy.arctan(b/a)
    
    return numpy.array((theta_x, theta_y, theta_z))
    