"""
Declares custom hand types with some mathematical processing
"""

__author__ = "George Xian"
__version__ = "0.1"
__email__ = "george_xian@live.com"
__status__ = "development"

# IMPORT MODULES
import collections
import numpy
import coord_transform as ct

# GLOBAL VARIABLES


# CLASS DEFINITIONS
class Appendage(object):
	"""
	Base abstract class for all elements in the Hands
	"""
	
	# FIELDS
	_pos = None
	
	# METHODS
	def __init__(self, history=1):
		self._vel_buf = collections.deque(maxlen=history)
		self._kernel = numpy.linspace(1, history+1, history)
		self._kernel /= numpy.sum(self._kernel) 
	
	@property
	def pos(self):
		"""Returns the last recorded position"""
		return self._pos
	
	@property
	def vel(self):
		"""Returns the last recorded velocity"""
		return self._vel_buf[-1]
	
	@pos.setter
	def pos(self, pos):
		"""Sets the last recorded position"""
		self._pos = pos
		
	@vel.setter
	def vel(self, vel):
		"""Adds the last recorded velocity to buffer"""
		self._vel_buf.append(vel)	# shift previous velocity into buffer
	
	@property	
	def wgt_avg_vel(self):
		"""Returns the weighted mean velocity"""
		return numpy.average(self._vel_buf, 0, self._kernel[0:len(self._vel_buf)])
		#return numpy.mean(self._vel_buf, 0)
	
	@property
	def sph_pos(self):
		"""Position in spherical co-ordinates"""
		return ct.cart2sph3D(self.pos)
	
	@property
	def sph_vel(self):
		"""Velocity in spherical co-ordinates"""
		return ct.cart2sph3D(self.vel)
	

class Finger(Appendage):
	"""
	Stores finger properties
	"""
	
	# FIELDS
	_drc = None
	_lng = None
	
	# METHODS
	@property
	def drc(self):
		"""Finger pointing direction"""
		return self._drc
	
	@property
	def lng(self):
		"""Finger length"""
		return self._lng
	
	@drc.setter
	def drc(self, drc):
		self._drc = drc
		
	@lng.setter
	def lng(self, lng):
		self._lng = lng
	
	@property
	def sph_dir(self):
		return ct.cart2sph3D(self.dir)


class Palm(Appendage):
	"""
	Stores the property of palms, has a list of references to fingers
	"""
	
	# FIELDS
	_fng = []
	_grip_rad = 0
	
	# METHODS	
	@property
	def fng(self):
		"""Finger list"""
		return self._fng
	
	@fng.setter
	def fng(self, fng):
		self._fng = fng
	
	@property
	def avg_fng_pos(self):
		return obj_avg(self.fng, lambda obj: obj.pos)
	
	@property
	def avg_fng_vel(self):
		return obj_avg(self.fng, lambda obj: obj.vel)
	
	@property
	def grip_rad(self):
		return self._grip_rad
	
	@grip_rad.setter
	def grip_rad(self, grip_rad):
		self._grip_rad = grip_rad


# FUNCTION DEFINITIONS
def obj_avg(objs, access):
	"""Finds the average vector in a list of objects, specify <access> so this
	function knows how to get the arrays out of each object"""
	
	vect_array = []
	
	for obj in objs:			# sum each element
		vect_array.append(access(obj))
		
	return numpy.mean(vect_array, 0)
				

# MAIN
if __name__ == "__main__":
	hand = Palm(5)
	hand.pos = numpy.array([1,2,3])
	hand.vel = numpy.array([3,2,1])
	
	hand.fng.append(Finger())
	hand.fng.append(Finger())
	hand.fng[0].pos = numpy.array([2,3,4])
	hand.fng[1].pos = numpy.array([3,4,5])
	hand.fng[0].vel = numpy.array([3,2,4])
	hand.fng[1].vel = numpy.array([1,0,2])
	print(hand.avg_fng_pos)
	print(hand.avg_fng_vel)
	