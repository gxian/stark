===========================================
		StarkPy 
===========================================
Date: 08-06-2014

	About StarkPy:
StarkPy is an experimental plugin for Blender to allow its sculpt tool to be interfaced via a LEAP motion controller. It is written in Python 3.3 and the recommended version of Blender is 2.70.


	Setting up developer environment:
As this project is developed in python, it is toolchain agnostic. The environment recommended is Eclipse with PyDev.

You'll need several python libraries to run these scripts:
-Numpy (should eventually port all numpy objects to blender's mathutil)
-Leap
-Blender libraries (bpy, mathutil) (should come pre-installed with Python)

The Leap python library is a Python 2.x wrapper for LEAP C code. You'll need to generate a Python 3.3 wrapper using SWIG and Visual Studio 2010 (had problems with 2012) with the instructions here: https://support.leapmotion.com/entries/39433657-Generating-a-Python-3-3-0-Wrapper-with-SWIG-2-0-9

You'll need an x86 wrapper for 32bit Blender and a x64 wrapper for 64bit Blender (instructions are for x64).

You'll also need to add the folder to your LEAP python libraries added to the PYTHONPATH environment variable. It is also recommended that you also add the folder containing this project to also be added to PYTHONPATH. 

Note that Eclipse will show errors on lines where bpy methods are called, this is not neccessarily an error, this is Eclipse not being able to see where bpy methods are defined. This is because they will only be valid inside Blender's environment.

	
	File structure:
--.project		
|
|
--.pydevproject
|
|
--coord_transform.py
|
|
--hand_types.py
|
|
--leap_operator.py
|
|
--panels.py
|
|
--readme.txt

All files prefixed with '.' are Eclipse configurations files, best to leave these.

coord_transform.py:
Contains various co-ordinate transform functions written for this project. Many of these are generic, and its best to replace them with ones from math libraries which should be faster.

hand_types.py:
Defines Palm and Finger classes which are used to store data coming from the Leap frame. As the lifetime of LEAP objects are volatile, information from the LEAP objects are transferred to instances of these classes to ensure collected data as certain lifetime. Also contains methods for computing smoothed velocity.

leap_opeator.py
Defines the modal operator object that defines the control flow of this plugin within Blender. Read more about operators at the offical Blender API documentaion: http://www.blender.org/documentation/blender_python_api_2_57_release/bpy.types.Operator.html

panels.py
Functions to setup panels and other GUI elements within Blender's GUI to configure paramters of the plugin during operation. Also defines defaults for these parameters.


	Notes on operation:
To run this script in Blender, you can either:
-Place these scripts in Blender startup folder
-Open leap_operator.py in Blender's script editor then running it.

This will place the required GUI elements in Blender. Such as the "LEAP Sculpt" button to start the sculpting session and the sliders to adjust parameters to suit your gesturing style.

You must press the "LEAP Sculpt" button whilst in sculpt mode on the selected object otherwise an error will result. You need the LEAP plugged in and activated, otherwise, nothing will happen. Read /StarkPaper/out/StarkPaper.pdf for the gesture language.


	Improvements to source code:
The bulk of actions that occur are made in the callback method 'modal' in the Sculpt_Operator class for leap_operator.py. This callback calls two functions: manipulate_viewport() and manipulate_tool() to perform viewport navigation and tool manipulation respectively. These functions will do the local->global co-ordinate mapping using matrix arithmetic via numpy and make Blender API calls to perform these actions. Future development should refactor these functions the make them shorter, and possibly call built-in operators to perform co-ordinate system mapping and actions within Blender to make it more context sensitive. 


