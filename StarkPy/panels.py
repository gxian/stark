"""
Defines the UI panels used for Blender's integration with LEAP's
module
"""

__author__ = "George Xian"
__version__ = "0.1"
__email__ = "george_xian@live.com"
__status__ = "development"

# IMPORT MODULES
import bpy
import bpy.props as bprop

# CLASS DEFINITIONS
class Tools_Panel(bpy.types.Panel):
    """
    Tool panel settings for the leap sculpting operator
    """
    
    bl_label = "LEAP Sculpt"
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'TOOLS'
    
    def draw(self, context):
        self.layout.operator("wm.leap_sculpt")
        

class UI_Panel(bpy.types.Panel):
    """
    UI panel layout for the leap sculpting operator
    """
    
    bl_label = "LEAP Sculpt settings"
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'UI'
    
    def draw(self, context):
        layout = self.layout
        scn = context.scene
        layout.prop(scn, 'FPS')
        layout.prop(scn, 'palm_hist')
        layout.prop(scn, 'cutoff_vel')
        layout.prop(scn, 'fing_len_crt')
        layout.prop(scn, 'trk_gain')
        layout.prop(scn, 'rot_gain')
        layout.prop(scn, 'scl_gain')
        layout.prop(scn, 'tool_gain')
        

# FUNCTION DEFINITONS        
def init_ui_prop(scn, FPS_default=10, palm_hist_default=1, cutoff_vel_default=80, fing_len_default=25, trk_gain_default=0.1, rot_gain_default=0.005, scl_gain_default=0.2, tool_gain_default=0.02):
    """
    Initializes properties for the UI panel settings, properties are
    contained in the Scene object
    """
            
    bpy.types.Scene.FPS = bprop.IntProperty(
        name = "FPS",
        description = "Rate to update hand input",
        min = 1,
        max = 300)
    
    bpy.types.Scene.palm_hist = bprop.IntProperty(
        name = "Smoothing",
        description = "Number of past iterations to average to perform smoothing",
        min = 1,
        max = 100)
    
    bpy.types.Scene.cutoff_vel = bprop.IntProperty(
        name = "Cutoff velocity",
        description = "Relative velocity of finger relative to palm to reject motion",
        min = 1,
        max = 1000)
    
    bpy.types.Scene.fing_len_crt = bprop.IntProperty(
        name = "Finger length",
        description = "Length of detected finger to be registered as a gesture",
        min = 1,
        max = 500)
    
    bpy.types.Scene.trk_gain = bprop.FloatProperty(
        name = "Tracking Sensitivity",
        description = "Movement sensitivity of the tracking gesture",
        default = trk_gain_default,
        min = 0.0001,
        max = 0.5)
    
    bpy.types.Scene.rot_gain = bprop.FloatProperty(
        name = "Tumbling Sensitivity",
        description = "Movement sensitivity of the tumbling gesture",
        default = rot_gain_default,
        min = 0.0001,
        max = 0.5)
    
    bpy.types.Scene.scl_gain = bprop.FloatProperty(
        name = "Dollying Sensitivity",
        description = "Movement sensitivity of the zooming gesture",
        default = scl_gain_default,
        min = 0.01,
        max = 5)
    
    bpy.types.Scene.tool_gain = bprop.FloatProperty(
        name = "Tool Sensitivity",
        description = "Movement sensitivity  of the tool",
        default = tool_gain_default,
        min = 0.0001,
        max = 0.5)
    
    # if you don't assign them dictionary values, you can't access them in dictionary
    scn['FPS'] = FPS_default
    scn['palm_hist'] = palm_hist_default
    scn['cutoff_vel'] = cutoff_vel_default
    scn['fing_len_crt'] = fing_len_default 
    scn['trk_gain'] = trk_gain_default
    scn['rot_gain'] = rot_gain_default
    scn['scl_gain'] = scl_gain_default
    scn['tool_gain'] = tool_gain_default
    

def register():
    bpy.utils.register_class(Tools_Panel)
    bpy.utils.register_class(UI_Panel)
    
def unregister():
    bpy.utils.unregister_class(Tools_Panel)
    bpy.utils.unregister_class(UI_Panel)
    