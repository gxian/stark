"""
Handles registration of operator class for Blender integration with
LEAP's module
"""

__author__ = "George Xian"
__version__ = "0.1"
__email__ = "george_xian@live.com"
__status__ = "development"

# IMPORT MODULES
import imp
import numpy
import mathutils as mt
import coord_transform as ct

import bpy
import Leap

import panels
import hand_types

imp.reload(panels)
imp.reload(hand_types)


# CLASS DECLARATION        
class Sculpt_Operator(bpy.types.Operator):
    """
    Operator for LEAP sculpting
    """

    bl_idname = "wm.leap_sculpt"
    bl_label = "LEAP Sculpt"
    
    _updating = False   # prevents operations when calculations are still being made
    _timer = None       # timer control when LEAP frames are polled
    
    def __init__(self):
        # initiate the leap controller
        self._controller = Leap.Controller()
        
        # initialize other parameters for this operator
        self._tim_add = 1/bpy.context.scene['FPS']
        
        # initialize relative cursor position and orientation from camera
        self.rel_cursor_pos = mt.Vector((4, 0, -15))
        self.rel_cursor_rot = mt.Quaternion((1, 0, 0, 0))
        
        self.stroke = [{'name': 'LEAP_Sculpt_Brush', 'mouse': (0,0), 'time':1, 'pen_flip':True}]
        
        # create objects to represent hands received from the Leap
        self.LHand = None
        self.RHand = None
        
        # store which object was selected
        self.obj_selected = bpy.context.object
        
        # create the cursor as a object in the view port
        bpy.data.scenes["Scene"].tool_settings.unified_paint_settings.size = 15
        bpy.ops.mesh.primitive_cone_add(location=(0,0,0), radius1=0.2)
        bpy.context.object.name = "ob_tx0x00"     
        bpy.ops.object.mode_set(mode = 'EDIT')
        bpy.ops.transform.rotate(value=-numpy.pi/2, axis=(1,0,0))   # make it point along y-axis
        bpy.ops.transform.translate(value=(0,-1.8,0.95))
        bpy.ops.object.mode_set(mode = 'OBJECT')
        
        # must be in sculpt mode to sculpt
        bpy.data.objects[self.obj_selected.name].select = True   # select the object to be sculpted
        bpy.ops.object.mode_set(mode = 'SCULPT') 
        
        # create context override 
        #TODO: Reduce the hackyness
        for window in bpy.context.window_manager.windows:
            screen = window.screen 
            for area in screen.areas:
                if area.type == 'VIEW_3D':
                    for region in area.regions:
                        if region.type == 'WINDOW':
                            self.override =  {'window': window, 'screen': screen, 'area': area, 'region': region, 'active_object': bpy.data.objects[self.obj_selected.name]}
                            break                                       
        
        
        print("sculpt operator: initiated")
        
    def __del__(self):
        # delete cursor object
        bpy.ops.object.mode_set(mode = 'OBJECT')
        bpy.data.objects[self.obj_selected.name].select = False
        bpy.data.objects['ob_tx0x00'].select = True
        bpy.ops.object.delete()
        
        print("sculpt operator terminated")
        
    def modal(self, context, event):
        if event.type == 'TIMER' and not self._updating:
            self._updating = True
            frame = self._controller.frame()
            self.LHand = get_hand_data(context, frame, 'left', self.LHand)
            self.RHand = get_hand_data(context, frame, 'right', self.RHand)
            
            # change perspective according to LEAP frames
            manipulate_viewport(context, self.LHand)
            
            # apply sculpt operation according to LEAP frames
            (self.rel_cursor_pos, self.rel_cursor_rot, self.stroke) = manipulate_tool(context, self.RHand, self.rel_cursor_pos, self.rel_cursor_rot, self.stroke, self.override)    
          
            self._updating = False
        
        elif event.type in {'LEFTMOUSE', 'ENTER'}:
            # confirm operation
            return self.accept(context)
        
        elif event.type in {'RIGHTMOUSE', 'ESC'}:
            # cancel operation
            return self.cancel(context)
        
        return {'RUNNING_MODAL'}
        
    def execute(self, context):
        # re-schedule the next frame polling event
        self._timer = context.window_manager.event_timer_add(self._tim_add, context.window)
        
    def invoke(self, context, event):
        # TODO: Timer events probably need to do nothing
        
        self.execute(context)
        
        print(context.window_manager.modal_handler_add(self))
        return {'RUNNING_MODAL'}
    
    def accept(self, context):
        context.window_manager.event_timer_remove(self._timer)
        self._timer = None
        print("sculpt operation accepted")
        return {'FINISHED'}
        
    
    def cancel(self, context):
        context.window_manager.event_timer_remove(self._timer)
        self._timer = None
        print("sculpt operation cancelled")
        return {'CANCELLED'}   
     
        
# FUNCTION DEFINITONS
def get_hand_data(context, frame, side, prev_hand=None):
    """
    Returns the hand data of the side requested in hand_types.Palm
    object, or None if no hand was found that fits the criteria
    """
    
    # get hand on chosen side and create a test to verify which side its on
    if side == 'left':
        hand = frame.hands.leftmost
        x = hand.palm_position.x
        
        is_correct_side = lambda x: x<=0
        
    elif side == 'right':
        hand = frame.hands.rightmost
        x = hand.palm_position.x
        
        is_correct_side = lambda y: x>0
        
    else:
        raise ValueError("'{0}' is an incorrect value for <side>".format(side))
    
    
    # return a hand_types.Palm is the chosen side's is valid, None if not
    if hand.is_valid and is_correct_side(x):
        # record position and velocity
        p = numpy.array((x, hand.palm_position.y, hand.palm_position.z))
        v = numpy.array((hand.palm_velocity.x, hand.palm_velocity.y, hand.palm_velocity.z))
        gr = hand.sphere_radius
        
        # get fingers
        f = get_fingers(hand)
        
        # if hand was previously invalid, create new Palm object
        if prev_hand is None:
            ret = hand_types.Palm(history=context.scene['palm_hist'])
        else:
            ret = prev_hand
        # adding properties to update hand object
        ret.pos, ret.vel, ret.fng, ret.grip_rad = p, v, f, gr
        
        return ret
    else:
        return None
        

def get_fingers(hand):
    """
    Returns a list of fingers from a Leap.Hand object 
    """
    
    f = []
    for finger in hand.pointables:
        # create shorter names for finger attributes
        tip_pos = finger.tip_position
        tip_vel = finger.tip_velocity
        tip_dir = finger.direction
        
        # recording attributes to new finger object
        new_finger = hand_types.Finger(history=1)
        new_finger.pos = numpy.array((tip_pos.x, tip_pos.y, tip_pos.z)) 
        new_finger.vel = numpy.array((tip_vel.x, tip_vel.y, tip_vel.z))
        new_finger.dir = numpy.array((tip_dir.x, tip_dir.y, tip_dir.z))
        new_finger.drc = numpy.array((finger.direction.x, -finger.direction.z,  finger.direction.y))
        new_finger.lng = finger.length        
        
        f.append(new_finger)
        
    return f

def all_finger_length(fingers, access, length):
    """
    Determine whether all fingers are longer than specified length
    """ 
    
    for finger in fingers:
        if access(finger) < length:
            return False
    
    return True


def manipulate_viewport(context, hand_data):
    """
    Apply actions to blender to manipulate the view port
    """
    
    dt = 1/context.scene['FPS']             # get time step 
    cutoff_vel = context.scene['cutoff_vel']# cutoff for finger velocity relative to palm velocity to reject motion
    fing_len = context.scene['fing_len_crt']# minimum length of detected finger to register gesture 
    
    view = context.space_data.region_3d     # get handler for the view port from context
    
    if hand_data is not None:
        rel_fng_palm_vel = numpy.linalg.norm(hand_data.avg_fng_vel-hand_data.vel, 2)
        if len(hand_data.fng) == 2 and rel_fng_palm_vel < cutoff_vel and all_finger_length(hand_data.fng, lambda x: x.lng, fing_len):
            # tracking gesture
            trk_gain = context.scene['trk_gain']

            ct_mat = numpy.matrix(view.view_rotation.to_matrix()) # transform local to global co-ords
            trck_vect = numpy.matrix(hand_data.wgt_avg_vel)       # data from leap
            trck_amount = mt.Vector(dt*trk_gain*ct_mat*trck_vect.T)
            view.view_location -= trck_amount # apply the transform
            print("New location: ", view.view_location)
            
            # determine dollying component
            diff_pos_vec = hand_data.fng[0].pos - hand_data.fng[1].pos
            diff_pos_qua = mt.Quaternion(ct.quat_between(numpy.array((1,0,0)), diff_pos_vec))
            diff_pos_qua.normalize()
            rot_matrix = diff_pos_qua.to_matrix()
            dolly_amount = rot_matrix*(mt.Vector(hand_data.fng[0].wgt_avg_vel) - mt.Vector(hand_data.fng[1].wgt_avg_vel))
            view.view_distance += dt*context.scene['scl_gain']*dolly_amount.x
            print("Dollyed: ", dolly_amount.x)
            
        
        elif len(hand_data.fng) == 1 and rel_fng_palm_vel < cutoff_vel and all_finger_length(hand_data.fng, lambda x: x.lng, fing_len):    
            # tumbling gesture
            tumb_step = dt*context.scene['rot_gain']        # constant alias
            raw_tumb_vect = hand_data.wgt_avg_vel           # analyze LEAP data
            
            # tumble x around the local axis
            tumb_pwr_x = tumb_step*raw_tumb_vect[1]
            tumb_qua_x = mt.Quaternion((tumb_pwr_x, -1, 0, 0))
            tumb_qua_x.normalize()
            # TODO: Multiplying once causes flipping, investigate Quaternion algebra
            after_tumb_x = view.view_rotation * tumb_qua_x * tumb_qua_x
            
            # tumble z around the global axis
            tumb_pwr_z = tumb_step*raw_tumb_vect[0]
            tumb_qua_z = mt.Quaternion((tumb_pwr_z, 0, 0, 1))
            tumb_qua_z.normalize()
            # TODO: Multiplying once causes flipping, investigate Quaternion algebra
            after_tumb_z = tumb_qua_z * tumb_qua_z * after_tumb_x
            
            # apply the operation
            view.view_rotation = after_tumb_z
            print("New orientation: ", view.view_rotation)
    
    else:
        print("no left hand found")
        
    
def manipulate_tool(context, hand_data, rel_pos_to_cam, rel_rot_to_cam, stroke_prop, overrider):
    """
    Apply actions in blender to operate sculpting tool
    """
    
    # constant aliasing
    dt = 1/context.scene['FPS']             # get time step 
    TOOL_CUTOFF_VEL = 20                    # cutoff for finger velocity relative to palm velocity to reject motion
    fing_len = context.scene['fing_len_crt']# minimum length of detected finger to register gesture
    trk_gain = context.scene['tool_gain']   # sensitivity to hand movements
    
    # object aliasing
    cursor = context.scene.objects["ob_tx0x00"]  # tool cursor
    view = context.space_data.region_3d     # get handler for the view port from context
    
    ct_mat = view.view_rotation.to_matrix() # co-ordinate transform matrix
    
    # move tool cursor to align with camera
    cam_dist_glob = ct_mat * mt.Vector((0, 0, view.view_distance))                  # camera distance alignment
    cam_align_tool_pos = view.view_location + cam_dist_glob + ct_mat*rel_pos_to_cam # camera position alignment
    cam_align_tool_rot = view.view_rotation                                         # camera rotation alignment  
    
    apply_stroke = False  
    
    if hand_data is not None and len(hand_data.fng) > 0 and numpy.linalg.norm(hand_data.avg_fng_vel-hand_data.vel, 2) > TOOL_CUTOFF_VEL and all_finger_length(hand_data.fng, lambda x: x.lng, fing_len):        
        # move tool cursor based on hands
        trck_vect = mt.Vector(hand_data.fng[0].wgt_avg_vel)     # data from leap
        pos_change = dt*trk_gain*trck_vect                      # step change in position    
        new_rel_pos_to_cam = rel_pos_to_cam + pos_change        # change the previous tool's relative camera position
    
        # orientate tool to reflect finger pointing direction
        fing_pnt_dir = hand_data.fng[0].drc
        new_rot = mt.Quaternion(ct.quat_between(numpy.array((0,1,0)), fing_pnt_dir))
        new_rot.normalize()
        
        # schedule apply sculpt stroke task
        apply_stroke = True
             
    else:
        # no right hand, relative position and orientation unchanged
        new_rel_pos_to_cam = rel_pos_to_cam     
        new_rot = rel_rot_to_cam
        
        # identity transform vectors because no change will occur to relative tool position and orientation 
        pos_change = mt.Vector((0,0,0))  
        print("no right hand found")
        
    # actually perform the transform
    cursor.rotation_mode = 'QUATERNION'
    cursor.location = cam_align_tool_pos + pos_change  
    cursor.rotation_quaternion = cam_align_tool_rot * mt.Quaternion((2,-1, 0, 0)) * new_rot
    
    # apply sculpt stroke
    if apply_stroke:
        if not stroke_prop[0]['is_start']:
            stroke_prop[0]['is_start'] = True
        else:
            stroke_prop[0]['is_start'] = False   
            
        stroke_prop[0]['location'] = cursor.location
        print("Sculpt location: ", stroke_prop[0])
        stroke_prop[0]['mouse'] = (0,0) 
        stroke_prop[0]['pressure'] = 1.0
        
        print(stroke_prop)
        
        bpy.ops.sculpt.brush_stroke(overrider, stroke=stroke_prop)
    else:
        stroke_prop[0]['is_start'] = False
        
        
    return (new_rel_pos_to_cam, new_rot, stroke_prop)
    

def register():
    """
    Adds custom plugins to Blender's interface
    """
    
    bpy.utils.register_class(Sculpt_Operator)
    panels.init_ui_prop(bpy.context.scene, FPS_default=25, palm_hist_default=5)
    panels.register()
    
    
def unregister():
    """
    Removes custom plugins to Blender's interface
    """
    
    bpy.utils.unregister_class(Sculpt_Operator)
    panels.unregister()
    

# MAIN
if __name__ == "__main__":
    register()
    